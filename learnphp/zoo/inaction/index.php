<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<style type="text/css">
@import url(style.css);
</style>
<title>PHP in Action!</title>
</head>
<body>
<main>
<img src="http://i1061.photobucket.com/albums/t480/ericqweinstein/php-logo_zps408c82d7.png" alt="PHP Logo">
<div class="header"><h1>
<?php
    $welcome = "Let's get started with PHP!";
    echo $welcome;
?>
</h1></div>
<p><strong>Generate a list:</strong>
<?php
    for ($number = 1; $number <= 10; $number++) {
        if ($number <= 9) {
            echo $number . ", ";
        } else {
            echo $number . "!";
        }
    };
?>
</p>
<p><strong>Things you can do:</strong></p>
<ul>
<?php
    $things = array("Talk to databases",
    "Send cookies", "Evaluate form data",
    "Build dynamic webpages");
    foreach ($things as $thing) {
        echo "  <li>$thing</li>\n";
    }
        
    unset($thing);
?>
</ul>
<p>
<strong>This jumbled sentence will change every time you click Submit!</strong>
</p>
<p>
<?php
    $words = array("the ", "quick ", "brown ", "fox ",
    "jumped ", "over ", "the ", "lazy ", "dog ");
    shuffle($words);
    foreach ($words as $word) {
        echo $word;
    };
        
    unset($word);
?>
</p>
</main>
<footer>
<a href="http://validator.w3.org/check/referer">
<strong> HTML </strong> Valid! </a> | 
<a href="http://jigsaw.w3.org/css-validator/check/referer?profile=css3">
<strong> CSS </strong> Valid! </a>
</footer> 
</body>
</html>
