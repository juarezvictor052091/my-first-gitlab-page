<?php include("top.inc"); ?>

<main>
<h2>About PHP</h2>
<p>
PHP is a programming language designed for
<a href="https://en.wikipedia.org/wiki/Server-side_scripting">
server-side scripting</a> on the
<a href="https://en.wikipedia.org/wiki/World_Wide_Web">web</a>.
</p>

<h2>Some Links to PHP Learning Resources</h2>
<ul>
  <li>
    <a href="https://www.codecademy.com/learn/php">Codeacademy PHP Course</a>
  </li>
  <li><a href="http://www.w3schools.com/php/">w3schools PHP 5 Tutorial</a></li>
</ul>
</main>

<?php include("bottom.inc"); ?>
