<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Getting Started with PHP</title>
<base href="http://students.gctaa.net/~jelkner/learnphp/">
<link rel="stylesheet" type="text/css" href="learnphp.css">
</head>

<body>
<header>
  <h1>Learning PHP</h1>
  <div id="quote">
  <?php include("quotes.php") ?>
  </div>
</header>

<?php include("menu.php") ?>
