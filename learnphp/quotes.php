<?php $Quotes = array(
  array("As we enjoy great Advantages from the Inventions of others, we should
         be glad of an Opportunity to serve others by any Invention of ours,
	 and this we should do freely and generously.",
	"Benjamin Franklin"),

  array("Education is our passport to the future, for tomorrow belongs to those 
         who prepare for it today.",
        "Malcolm X"),

  array("He that does good to another does good also to himself.", "Seneca"),

  array("Beware of he who would deny you access to information, for in his
         heart he dreams himself your master.",
        "Pravin Lal"),

  array("When did the copyright holders become the Corporations?  Who owns the 
         Intellectual Property?  Who controls our Arts and Sciences?  They are
         slowly eroding our rights. The authors, scientists, researchers,
         students and you are very much at risk.", "Anonymous"),

  array("Imagine what a harmonious world it could be if every single person,
         both young and old shared a little of what he is good at
         doing.", "Quincy Jones"),

  array("The duty of helping one's self in the highest sense involves the
         helping of one's neighbors.", "Samuel Smiles"),

  array("Imagine a world in which every single person on the planet is given
         free access to the sum of all human knowledge.", "Jimmy Wales,
         Founder of Wikipedia"),

  array("It is in fact nothing short of a miracle that the modern methods of
         instruction have not yet entirely strangled the holy curiosity of 
         inquiry; for what this delicate little plant needs more than anything, 
         besides stimulation, is freedom.", "Albert Einstein"),

  array("It is a very grave mistake to think that the enjoyment of seeing and
         searching can be promoted by means of coercion and a sense of duty.",
         "Albert Einstein"),

  array("A tree is known by its fruit; a man by his deeds. A good deed is never 
         lost; he who sows courtesy reaps friendship, and he who plants
         kindness gathers love.", "St. Basil"),

  array("Find a job you love and you'll never work a day in your life.",
        "Confucius"),

  array("Not everything that can be counted counts, and not everything that
         counts can be counted.", "Albert Einstein"),

  array("There are certainly cases where applying objective measures badly is
         worse than not applying them at all, and education may well be one of
         those.", "Nate Silver")
);

$len = count($Quotes);
$index = rand(0, $len-1);
?>

<p class="quote">
<q><?php echo $Quotes[$index][0]; ?></q>
</p>
<p class="author">
-- <?php print $Quotes[$index][1]; ?>
</p>
